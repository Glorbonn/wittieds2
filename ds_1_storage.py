import csv, ds_1_input, boto3, botocore

'''Reads the node_id local log file and returns a list of dictionary entries.'''
def from_file(node_id):
    f = open("node_" + str(node_id) +"_file", "r")
    content = f.readlines()
    dicts = []

    for line in content:
        stripped = line.rstrip("\n")
        dicts.append(eval(stripped))
    f.close()
    return dicts

'''Adds the appointment object in_obj to the node_id local log file.'''
def to_file(in_obj, node_id):
    f = open("node_" + str(node_id) +"_file", "a")

    if isinstance(in_obj, dict):
        dict_str = str(in_obj)
        #print(dict_str)
        f.write(dict_str + "\n")
    elif isinstance(in_obj, str):
        f.write(in_obj + "\n")
        #print(in_obj)

    f.close()

'''A function that can be used to overwrite all files as blanks. Helps when debugging the program, where one wants to restart from a blank calendar slate.'''
def clear_file(node_id):
    f = open("node_" + str(node_id) +"_file", "w").close()

'''This function was called to set up s3 buckets. It was only called once, and then commented out (as in the current code).'''
def init_s3(region_self):
    self_bucket = boto3.resource('s3')
    if region_self == 'us-east-1':
        self_bucket.create_bucket(Bucket='bigboi-unique-bucket-ds-spring-2018-bucket' + region_self)
    else:
        bucket = self_bucket.create_bucket(CreateBucketConfiguration = {'LocationConstraint' : region_self}, Bucket='bigboi-unique-bucket-ds-spring-2018-bucket' + region_self)

'''The function send a node_id local log file to the S3 bucket using the appropriate region'''
def to_s3(region_self, node_id):
    s3 = boto3.resource('s3')
    self_s3_client = boto3.client('s3')
    f = open("node_" + str(node_id) +"_file", "r")
    s3.Bucket('bigboi-unique-bucket-ds-spring-2018-bucket' + region_self).put_object(Key="reliable_file", Body=f)

'''The function overwrites the node_id local log file with the S3 file.'''
def from_s3(region_self, node_id):
    s3 = boto3.resource('s3')

    BUCKET_NAME = 'bigboi-unique-bucket-ds-spring-2018-bucket' + region_self # replace with your bucket name
    KEY = 'reliable_file' # replace with your object key

    s3 = boto3.resource('s3')

    try:
        s3.Bucket(BUCKET_NAME).download_file(KEY, "node_" + str(node_id) +"_file")
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

'''A function used for testing.'''
def tester_func(region_self):
    clear_file()
    dict1 = ds_1_input.get_appt_input().to_dict(1)
    dict2 = ds_1_input.get_appt_input().to_dict(1)

    to_file(dict1)
    to_file(dict2)

    read_dicts = from_file()
    #init_s3(region_self)

    to_s3(region_self)

    from_s3(region_self)

    #print(read_dicts)

#tester_func()
