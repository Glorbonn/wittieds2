from sets import Set
import ds_1_input, ds_1_message, ds_1_storage, sys, threading, time, Queue
from random import randint

# Notes:
# Vote request pauses thread, send vote reply pauses thread

# Message received pauses thread

# Program Global Vars
thread_tickrate = 40.0 #thread tickrate in ms
input_thread_tickrate = 100.0
heartbeat_wait_duration = 250.0 #heartbeat tickrate in ms
candidate_max_time_thresh = 8500.0
candidate_min_time_thresh = 2500.0

follower_max_time_thresh = 11000.0
follower_min_time_thresh = 6500.0

candidate_timeout = float(randint(candidate_min_time_thresh, candidate_max_time_thresh)) / float(1000)
follower_timeout = float(randint(follower_min_time_thresh, follower_max_time_thresh)) / float(1000)

#print("Candidate timeout is: " + str(candidate_timeout) + " \n")

sentVoteForTerm = False
votes_sent = Set()

possibleLeaders = [0,1,2,3,4]

currentCommandTerm = 0

start_time = 0
null_value = 'n' # "null" value that is used to represent an empty value in the RaftMessage class

alreadyWonSet = Set()
alreadyWonCommandSet = Set()

candidateEntriesQueue = Queue.Queue() # Queue for when receive client command in candidate, we then deal with them if we become leader, otherwise send to leader
voters_received = Set() # Set of voters who have sent votes
command_voters_received = Set()
messages = Queue.Queue() # Blocking Queue that received messages are put into
inputs = Queue.Queue() # Blocking Queue that inputs are put into
logEntriesQueue = Queue.Queue() # Queue where committed attacks/ blocks are put into, for reading and feedback in the input (player) thread
leaderIdQueue = Queue.Queue() # Blocking queue for updating leader id. Implemented to ever only have one value
#sending_queue = Queue.Queue() # Blocking Queue that sending messages are put into
runQueue = Queue.Queue()
timeoutQueue = Queue.Queue()

votes_received = 1 # Node starts with one vote for itself for use in counting votes in candidate state
command_votes_received = 1
majority_vote = 3 # Hardcoded for 5 nodes

timeout_flag = False # flag forces timeout when seen in noTimeout() function
run_flag = True # flag, when switched to false, does not trigger main while loops in raft and receive threads
state = "FOLLOWER" #starting state of RAFT

player_self_id_global = null_value

nodes = {} # Dict that all known nodes will be added two in controller()
lastTimestamp = 0
# RAFT Volatile state for all servers
commitIndex = 0
lastApplied = 0
currentTerm = 0
# RAFT Volatile state for use if leader
nextIndex = []
matchIndex =[]
node_1_timestamp = 0
node_2_timestamp = 0
node_3_timestmap = 0
node_4_timestamp = 0
node_5_timestamp = 0

#leader_id = "n"




def clearfile():
    for x in range(0,4):
        ds_1_storage.clear_file(x)

'''This is the main driver function. It takes in parameters from the command line, which are the id for each node, and the region associated with each id.
Then, the function starts a receive thread that runs every 3 seconds (this is the Wuu-Bernstein receive, essentially). Also, the function loops for more input
from the command line interface, which is used to add, view, and remove appointments. Current appointments for relevant participants are printed out to help the user
find a free slot.'''
def controller(self_region, region1, region1_id, region2, region2_id, region3, region3_id, region4, region4_id, self_id, player_1_region, player_1_id, player_2_region, player_2_id, player_self_id):
    #nodes.append() {'region1_id':region1, 'region2_id':region2, 'region3_id':region3}
    nodes.update({region1_id:region1})
    nodes.update({region2_id:region2})
    nodes.update({region3_id:region3})
    nodes.update({region4_id:region4})
    nodes.update({self_id:self_region})
    receive(self_region, self_id)
    #send(self_region, self_id)

    global timeout_flag
    global run_flag
    global input_thread_tickrate
    global logEntriesQueue
    global leader_id
    global player_self_id_global
    #global leader_region
    player_self_id_global = player_self_id

    ds_1_storage.clear_file(self_id)
    ds_1_storage.to_s3(self_region, self_id)
    print("File is clear, checking file contents : ")
    ds_1_storage.from_s3(self_region, self_id)
    print(ds_1_storage.from_file(self_id))
    #ds_1_storage.init_s3(nodes[self_id])

    #clearMessages(self_region, self_id)

    the_input_thread = threading.Thread(target = input_thread, args = [self_id, player_self_id])

    raft_thread = threading.Thread(target = run_raft, args = self_id)
    #raft_thread.daemon = True
    raft_thread.start()
    the_input_thread.start()

    if (player_self_id is not "none"):
        print("Playing as player " + str(player_self_id))


        #time.sleep(input_thread_tickrate / 1000.0)

    #run_raft(self_id)

def input_thread(self_id, player_self_id):
    global run_flag
    global timeout_flag
    while(True):
        input_str = raw_input("Options: (f) fail, (r) recover, (t) timeout, (s) block right, (a) block left, (w) attack right, (q) attack left\n")
        if (input_str == "f"):
            print("Node failure.\n")
            run_flag = False
        if (input_str == "r"):
            timeout_flag = False
            run_flag = True
            ds_1_storage.from_s3(nodes[self_id], self_id)
            print("Node recovery.\n")
            print("Received log from secure storage: \n")
            print(ds_1_storage.from_file(self_id))
        if (input_str == "t"):
            timeout_flag = True
            print("Node timeout.\n")
        if (input_str == "s" and player_self_id is not "none"):
            if player_self_id == "1":
                block_right(player_1_id)
            elif player_self_id == "2":
                block_right(player_2_id)
            print("Right side block command entered.")
        elif (input_str == "a" and player_self_id is not "none"):
            if player_self_id == "1":
                block_left(player_1_id)
            elif player_self_id == "2":
                block_left(player_2_id)

            print("Left side block command entered.")
        elif (input_str == "w" and player_self_id is not "none"):
            if player_self_id == "1":
                attack_right(player_2_id)
            elif player_self_id == "2":
                attack_right(player_1_id)

            print("Right side attack command entered.")
        elif (input_str == "q" and player_self_id is not "none"):
            if player_self_id == "1":
                attack_left(player_2_id)
            elif player_self_id == "2":
                attack_left(player_1_id)

            print("Left side attack command entered.")

        #getFeedback(logEntriesQueue, player_self_id)



'''def attack(player_region, player_id):
    # Need to make sure these are queued to send to the leader
    # Make raft message that sets AttackEvent flag on player 2, set from_player addr player_address @ player_region in raft message, to help route back responses from leader
    # Make raft message that sets AttackEvent flag on player 1

def block(player_id, player_region, player_address):
    # Need to make sure these are qeued to send to the leader
    # Make raft message that sets BlockEvent flag on player 1
    # Make raft message that sets BlockEvent flag on player 2'''

'''Sends a block left command to the leader, blocking player_id on the left'''
def block_left(player_id):
    # Need block_left flag, block_right flag that take in player_id
    # Need attack_left flag, attack_right flag that take in player_id
    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
             entries, term, leader_region, leader_id, attack_left_event,
             attack_right_event, block_left_event, block_right_event, from_player_region,
             from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
             command_timestamp, win_flag, commit_flag)'''
    block_left_flag = player_id
    block_left_message_str = RaftMessage(null_value, null_value, null_value,
                                         null_value, null_value, null_value,
                                         null_value, null_value, null_value,
                                         null_value, block_left_flag, null_value,
                                         null_value, null_value, 1,
                                         null_value,null_value, lastTimestamp,
                                         null_value, null_value).to_dict()
    retryMessage(block_left_message_str)
    print("Sending block left command.")
    '''while(leaderIdQueue.empty()):
        pass
    else:
        ds_1_message.send_message(nodes[leader_id], block_message_str)'''
    return

'''Sends a block right command to the leader, blocking player_id on the right'''
def block_right( player_id):
    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
             entries, term, leader_region, leader_id, attack_left_event,
             attack_right_event, block_left_event, block_right_event, from_player_region,
             from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
             command_timestamp, win_flag, commit_flag)'''
    block_right_flag = player_id
    block_right_message_str = RaftMessage(null_value, null_value, null_value,
                                          null_value, null_value, null_value,
                                          null_value, null_value, null_value,
                                          null_value, null_value, block_right_flag,
                                          null_value, null_value, 1,
                                          null_value, null_value, lastTimestamp,
                                          null_value, null_value).to_dict()
    #ds_1_message.send_message(nodes[leader_id], block_message_str)
    retryMessage(block_right_message_str)
    return

'''Sends an attack left command to the leader, attacking player_id on the left'''
def attack_left( player_id):
    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
             entries, term, leader_region, leader_id, attack_left_event,
             attack_right_event, block_left_event, block_right_event, from_player_region,
             from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
             command_timestamp, win_flag, commit_flag)'''
    attack_left_flag = player_id
    attack_left_msg_str = RaftMessage(null_value, null_value, null_value,
                                      null_value, null_value, null_value,
                                      null_value, null_value, attack_left_flag,
                                      null_value, null_value, null_value,
                                      null_value, null_value, 1,
                                      null_value, null_value, lastTimestamp,
                                      null_value, null_value).to_dict()
    #ds_1_message.send_message(nodes[leader_id], block_message_str)
    retryMessage(attack_left_msg_str)
    #print("Sending attack left command.")
    return

'''Sends an attack left command to the leader, attacking player_id on the right'''
def attack_right( player_id):
    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
                 entries, term, leader_region, leader_id, attack_left_event,
                 attack_right_event, block_left_event, block_right_event, from_player_region,
                 from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
                 command_timestamp, win_flag, commit_flag)'''
    attack_right_flag = player_id
    attack_right_msg_str = RaftMessage(null_value, null_value, null_value,
                                       null_value, null_value, null_value,
                                       null_value, null_value, null_value,
                                       attack_right_flag, null_value, null_value,
                                       null_value, null_value, 1,
                                       null_value, null_value, lastTimestamp,
                                       null_value, null_value).to_dict()
    #ds_1_message.send_message(nodes[leader_id], block_message_str)
    retryMessage(attack_right_msg_str)
    #print("Sending attack right command.")
    return

def retryMessage(message_str):
    while (leaderIdQueue.empty()):
        pass
    else:
        leader_id_list = list(leaderIdQueue.queue)
        leader_id = leader_id_list[0]
        print("Sending command.")
        ds_1_message.send_message(nodes[leader_id], message_str)
        return

def getLeaderId():
    leader_id_list = list(leaderIdQueue.queue)
    leader_id = leader_id_list[0]
    return leader_id

def getFeedback3(logEntriesQueue, player):
    output_log = list(logEntriesQueue.queue)
    print("Length of output_log is " + str(len(output_log)))
    # 0) Create a temporary list that has all log entries (we will only operate commands on this list, otherwise we will have the full log history)
    # 1) Check if log is length 1 and there is an attack on either left or right side, and end game
    # 2) When attack_left is added to end of log, if log entry with index > previous index has a block_left, remove block_left and attack_left
    # 3) Likewise for attack_right
    # 4) Output temporary list
    if len(output_log) == 1:
        print("Output log 0,0 is " + str(output_log[0][0]))
        if output_log[0][0] == "attack_right" or output_log[0][0] == "attack_left":
            if str(player) == str(output_log[0][1]):
                print("Attack hits unblocked Player 1. Player 1 has lost.")
                print("Player 2 has won.")
            elif str(player) != str(output_log[0][1]):
                print("Attack hits unblocked Player 2. Player 2 has lost.")
                print("Player 1 has won.")


    i = 0
    j = 1
    removedFlag = False
    #while (len(output_log) >= 2):
    while ( i <= len(output_log) -1  and i < j ):
        print("Length of output log is " + str(len(output_log)))
        print("i is " + str(i))
        print("j is " + str(j))

        '''if (len(output_log) <=2):
            break'''
        #element 0 is command
        #element 1 is target of that command
        #print("output_log i, 0 is " + str(output_log[i][0]))
        #print("output log i+1, 0 is " + str(output_log[i+1][0]))
        #print("Element i:")
        #print("command: " + str(output_log[i][0]))
        #print("target: " + str(output_log[i][1]))
        #print("timestamp: " + str(output_log[i][2]))
        while( j <= len(output_log) - 1):
            #print(j)

            #print("Element j:")
            #print("command: " + str(output_log[j][0]))
            #print("target: " + str(output_log[j][1]))
            #print("timestamp: " + str(output_log[j][2]))

            if output_log[i][0] == "block_left" and output_log[j][0] == "attack_left" and output_log[i][1] == output_log[j][1]:
                to_remove = output_log[i]
                to_remove2 = output_log[j]
                print("Attack blocked on left side.")
                output_log.remove(to_remove)
                output_log.remove(to_remove2)
                #del output_log[i]
                #output_log.remove(output_log[i])
                #output_log.remove(output_log[i])
                #print("Reset i = 0, j = 1")
                #time.sleep(3)
                removedFlag = True
                lastRemoved = output_log[i][1]
                i=0
                #j=1
                #break
            elif output_log[i][0] == "block_right" and output_log[j][0] == "attack_right" and output_log[i][1] == output_log[j][1]:
                to_remove = output_log[i]
                to_remove2 = output_log[j]
                print("Attack blocked on left side.")
                output_log.remove(to_remove)
                output_log.remove(to_remove2)
                #output_log.remove(output_log[i])
                #output_log.remove(output_log[i])
                #print("Reset i = 0, j = 1")
                #time.sleep(3)
                i=0
                removedFlag = True
                lastRemoved = output_log[i][1]
                #j=1
                #break
            else:
                removedFlag = False
                j += 1
        if removedFlag == True:
            i = 0
        else:
            i+=1
        j = i+1

        #j=i+1
            #print("incr j")
            #else:



        '''if (len(output_log) <=2):
            break'''

    print("Commands log: ")
    for line in output_log:
        print(str(line))


'''Reads log entries and prints feedback for player when consensus is reached on: hit left, hit right, block left, block right, or win/ loss.
The log entries have the form of the tuple (action, action_target, timestamp, isConsumed) where action is attack or block left or right, 
action_target is who that action applies to, the timestamp of the action, and whether the block is consumed (only used when action = block left or right'''
def getFeedback2(logEntriesQueue, player):
    output_log = list(logEntriesQueue.queue)
    print("Length of output_log is " + str(len(output_log)))
    # 0) Create a temporary list that has all log entries (we will only operate commands on this list, otherwise we will have the full log history)
    # 1) Check if log is length 1 and there is an attack on either left or right side, and end game
    # 2) When attack_left is added to end of log, if log entry with index > previous index has a block_left, remove block_left and attack_left
    # 3) Likewise for attack_right
    # 4) Output temporary list
    if len(output_log) == 1:
        print("Output log 0,0 is " + str(output_log[0][0]))
        if output_log[0][0] == "attack_right" or output_log[0][0] == "attack_left":
            if str(player) == str(output_log[0][1]):
                print("Attack hits unblocked Player 1. Player 1 has lost.")
                print("Player 2 has won.")
            elif str(player) != str(output_log[0][1]):
                print("Attack hits unblocked Player 2. Player 2 has lost.")
                print("Player 1 has won.")


    i = 0
    j = 1
    #while (len(output_log) >= 2):
    while ( i <= len(output_log) - 2 and len(output_log) > 1):
        print("Length of output log is " + str(len(output_log)))
        print("i is " + str(i))
        print("j is " + str(j))

        '''if (len(output_log) <=2):
            break'''
        #element 0 is command
        #element 1 is target of that command
        #print("output_log i, 0 is " + str(output_log[i][0]))
        #print("output log i+1, 0 is " + str(output_log[i+1][0]))
        print("Element i:")
        print("command: " + str(output_log[i][0]))
        print("target: " + str(output_log[i][1]))
        while(j > i and j <= len(output_log) - 1 and i <= len(output_log) - 2):



            print("Element j:")
            print("command: " + str(output_log[j][0]))
            print("target: " + str(output_log[j][1]))


            if output_log[i][0] == "block_left" and output_log[j][0] == "attack_left" and output_log[i][1] != output_log[j][1]:
                to_remove = output_log[i]
                to_remove2 = output_log[j]
                print("Attack blocked on left side.")
                output_log.remove(to_remove)
                output_log.remove(to_remove2)
                #del output_log[i]
                #output_log.remove(output_log[i])
                #output_log.remove(output_log[i])
                i=0
                j=1
            elif output_log[i][0] == "block_right" and output_log[j][0] == "attack_right" and output_log[i][1] != output_log[j][1]:
                to_remove = output_log[i]
                to_remove2 = output_log[j]
                print("Attack blocked on left side.")
                output_log.remove(to_remove)
                output_log.remove(to_remove2)
                #output_log.remove(output_log[i])
                #output_log.remove(output_log[i])
                i=0
                j=1
            else:
                j+=1
        else:
            i+=1

        '''if (len(output_log) <=2):
            break'''

    print("Commands log: ")
    for line in output_log:
        print(str(line))

    if len(output_log) == 1:
        print("Output log 0,0 is " + str(output_log[0][0]))
        if output_log[0][0] == "attack_right" or output_log[0][0] == "attack_left":
            if str(1) == str(output_log[0][1]):
                print("Attack hits unblocked Player 1. Player 1 has lost.")
                print("Player 2 has won.")
            elif str(2) == str(output_log[0][1]):
                print("Attack hits unblocked Player 2. Player 2 has lost.")
                print("Player 1 has won.")


def getFeedback(logEntriesQueue, player):
    if logEntriesQueue.empty():
        print("Log Entries queue is empty.")
        return
    log_list = list(logEntriesQueue.queue)
    print("Log list is " + str(log_list))
    if len(log_list) == 1:
        print("Log list is 1 element long")
        return
    print("Length of log list is " + str(len(log_list)))
    for index, item in enumerate(log_list):
        '''if index >= limit:
            break'''
        element = log_list[index]
        element_command = element[0]
        element_target = element[1]
        element_timestamp = int(element[2])
        print("element1: ")
        print("element1 command " + str(element_command))
        print("element1 target " + str(element_target))
        print("element1 timestamp " + str(element_timestamp))
        if element_command == "attack_left" and element_target == player:
            attack_tuple = element
            attack_tuple_command = element_command
            timestamp_attack = element_timestamp
            '''Check for any block left on player before the timestamp of this attack'''
            for index2, item2 in enumerate(log_list):
                print("In inner for loop 1")
                element2 = log_list[index2]
                element2_command = element2[0]
                element2_target = element2[1]
                element2_timestamp = int(element2[2])
                print("Element 2: ")
                print("element2 command: " + str(element2_command))
                print("element2 target " + str(element2_target))
                print("element2 timestamp " + str(element2_timestamp))
                if element2_command == "block_left" and element2_target == player and element2_timestamp <= timestamp_attack and element2[3] is False:
                    element2_consumed = True
                    element2[3] = element2_consumed
                    print("Player " + str(player) + " blocked an attack on the left.\n")
                elif element2_command == "block_left" and element2_target == player and element2_timestamp > timestamp_attack:
                    print("Player " + str(player) + " hit Player " + str(element2_target) + " on the left side.\n")# Consume left block
                # else if no block_left, die, and send update to leader.. will need to add another RaftMessage entry for when a player has died, which we will parse in log in this getFeedback loop
                # The player_loss_flag will be a tuple of the type (player_loss, 1, null_value, null_value), which means player 1 has lost. Depending on if player 1 or player 2 sees that message,
                # a different message will be displayed, being either "WIN" or "LOSS"
        elif element_command == "attack_right" and element_target == player:
            '''Check for any block right before the timestamp of this attack'''
            timestamp_attack = element_timestamp
            for index2, item2 in enumerate(log_list):
                print("In inner for loop 2")
                element2 = log_list[index2]
                element2_command = element2[0]
                element2_target = element2[1]
                element2_timestamp = int(element2[2])
                print("Element 2: ")
                print("element2 command: " + str(element2_command))
                print("element2 target " + str(element2_target))
                print("element2 timestamp " + str(element2_timestamp))
                if element2_command == "block_right" and element2_target == player and element2_timestamp <= timestamp_attack and element2[3] is False:
                    element2_consumed = True
                    element2[3] = element2_consumed
                    print("Player " + str(player) + " blocked an attack on the right.\n")
                elif element2_command == "block_right" and element2_target == player and element2_timestamp > timestamp_attack:
                    print("Player " + str(player) + " hit Player " + str(element2_target) + " on the right side.\n")
        '''Check if player was hit and did not block. If so, announce winner.'''
    if player == 1 and hasLost(player, log_list):
        print("Player 1 has lost.")
        print("Player 2 has won.")
    elif player == 2 and hasLost(player, log_list):
        print("Player 2 has lost.")
        print("Player 1 has won.")


def hasLost(player, log):
    allConsumed = False
    for index, item in enumerate(log):
        element = log[index]
        element_command = element[0]
        element_target = element[1]
        element_timestamp = element[2]
        element_consumed = element[3]

        if element_target == player:
            element2 = log[index2]
            element2_command = element2[0]
            element2_target = element2[1]
            element2_timestamp = element2[2]
            for index2, item2 in enumerate(log):
                if index2 > index:
                    if (element2_command == "attack_left" or element2_command == "attack_right") and allBlocksConsumed(player, log, index2-1):
                        return True
                    else:
                        return False

def allBlocksConsumed(player, log, end_index):
    for index, item in enumerate(log):
        element = log[index]
        element_command = element[0]
        element_target = element[1]
        element_timestamp = element[2]
        element_consumed = element[3]
        if index <= end_index and element_target == player:
            element_consumed = element[3]
            if element_consumed == False:
                return False
            else:
                return True

    # Read log entries queue (that has to have log entries put into it every time there is a commit
    # Parse log entries queue for whether player has hit, successfully blocked, been hit, or won/ lost the game

def sendAllCandidatesToLeader():
    pass
    #while (not candidateEntriesQueue.empty()):
    #   message = candidateEntriesQueue.get()


'''def sendAppendEntriesReply():
    pass'''

def rerouteToLeader(message):
    while (leaderIdQueue.empty()):
        pass
    else:

        leader_id_list = list(leaderIdQueue.queue)
        leader_id = leader_id_list[0]
        print("Leader ID is " + str(leader_id))
        ds_1_message.send_message(nodes[leader_id], message)
    return

def requestCommandVote(term, candidateID, lastLogIndex, lastLogTerm, self_id, message, timestamp):
    other_nodes = nodes.copy()

    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
                 entries, term, leader_region, leader_id, attack_left_event,
                 attack_right_event, block_left_event, block_right_event, from_player_region,
                 from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
                 command_timestamp, win_flag, commit_flag):'''
    attack_right_flag = message['attack_right_event']
    attack_left_flag = message['attack_left_event']
    block_left_flag = message['block_left_event']
    block_right_flag = message['block_right_event']


    del other_nodes[self_id]

    for node in other_nodes:
        request_command_vote_flag = 1
        #print("Request vote line passed.")
        request_vote_msg = RaftMessage(self_id, null_value, null_value,
                                       null_value, null_value, term,
                                       null_value, self_id, attack_left_flag,
                                       attack_right_flag, block_left_flag, block_right_flag,
                                       null_value, null_value, null_value,
                                       request_command_vote_flag, null_value, timestamp,
                                       null_value, null_value).to_dict()
        #print("Raft message line passed.")
        request_vote_msg_str = str(request_vote_msg)
        #print("Str conversion passed")
        #sending_queue.put((nodes[node], request_vote_msg_str))
        ds_1_message.send_message(other_nodes[node], request_vote_msg_str)
    pass



def sendCommandVoteReply(message, self_id):
    vote_reply_flag = 1
    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
                 entries, term, leader_region, leader_id, attack_left_event,
                 attack_right_event, block_left_event, block_right_event, from_player_region,
                 from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
                 command_timestamp, win_flag, commit_flag)'''
    attack_right_flag = message['attack_right_event']
    attack_left_flag = message['attack_left_event']
    block_left_flag = message['block_left_event']
    block_right_flag = message['block_right_event']
    command_timestamp = message['command_timestamp']
    vote_response = RaftMessage(self_id, null_value, null_value,
                                       null_value, null_value, null_value,
                                       null_value, null_value, attack_left_flag,
                                       attack_right_flag, block_left_flag, block_right_flag,
                                       null_value, null_value, null_value,
                                       null_value, vote_reply_flag, command_timestamp,
                                       null_value, null_value).to_dict()
    vote_response_str = str(vote_response)
    #sending_queue.put((candidate, vote_response_str))
    while (leaderIdQueue.empty()):
        pass
    else:
        leader_id_list = list(leaderIdQueue.queue)
        leader_id = leader_id_list[0]
        print("Leader is " + str(leader_id))
        print("Sending command.")
        ds_1_message.send_message(nodes[leader_id], vote_response_str)
    return
    #ds_1_message.send_message(candidate, vote_response_str)

def commandVoteGranted(message, currentTerm):
    global command_votes_received
    global command_voters_received
    if message['send_command_vote_flag'] == 1 and message['node_src'] not in command_voters_received:
        command_voters_received.add(message['node_src'])
        print("Received vote on CLIENT COMMAND. CURRENT VOTES: " + str(command_votes_received) + "\n")
        command_votes_received += 1
    if command_votes_received >= majority_vote and currentTerm not in alreadyWonCommandSet:
        alreadyWonCommandSet.add(currentTerm)
        print("VOTE WON on CLIENT COMMAND: Majority of votes received in term " + str(currentTerm) + "\n")
        command_votes_received = 1 #reset votes
        command_voters_received.clear()
        return True

def sendCommitResponse(message, timestamp,self_id):
    other_nodes = nodes.copy()
    del other_nodes[self_id]

    '''RaftMessage(node_src, request_vote_flag, send_vote_flag, append_entries_flag,
                 entries, term, leader_region, leader_id, attack_left_event,
                 attack_right_event, block_left_event, block_right_event, from_player_region,
                 from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
                 command_timestamp, win_flag, commit_flag))'''
    commit_flag = 1

    for node in other_nodes:
        attack_right_flag = message['attack_right_event']
        attack_left_flag = message['attack_left_event']
        block_left_flag = message['block_left_event']
        block_right_flag = message['block_right_event']
        vote_response = RaftMessage(self_id, null_value, null_value,
                                           null_value, null_value, null_value,
                                           null_value, self_id, attack_left_flag,
                                           attack_right_flag, block_left_flag, block_right_flag,
                                           null_value, null_value, null_value,
                                           null_value, null_value, timestamp,
                                           null_value, commit_flag).to_dict()
        vote_response_str = str(vote_response)
        #candidate = nodes[message['node_src']]
        #sending_queue.put((candidate, vote_response_str))
        ds_1_message.send_message(nodes[node], vote_response_str)

def updateLeader(message):
    if message['leader_id'] is not null_value:
        leader_id = message['leader_id']
        if int(leader_id) in possibleLeaders:
            print("Updating leader id to " + str(leader_id))
            leaderIdQueue.queue.clear()
            leaderIdQueue.put(leader_id)
    return


def run_raft(self_id):
    global leader_id
    global state
    global candidate_start_time
    global follower_start_time
    global candidate_timeout
    global follower_timeout
    global currentCommandTerm
    global messages
    global currentTerm
    global run_flag
    global lastTimestamp
    global votes_received
    global voters_received
    global command_votes_received
    global command_voters_received
    follower_start_time = time.time()
    print("***STARTING RAFT PROTOCOL***\n")
    #print("MESSAGES in run_raft WHILE loop are " + str(messages) + ".\n")

    while True:
        if run_flag == False:
            state = "FOLLOWER"
            votes_received = 1
            voters_received.clear()
            command_votes_received = 1
            command_voters_received.clear()
            resetFollowerTimeout()
            resetCandidateTimeout()
        while run_flag:
            if state == "FOLLOWER":
                if noTimeout(follower_timeout, follower_start_time):
                    while messages.empty() == False:
                        message = messages.get_nowait() #TRY TO MAKE THIS get_nowait()
                        #messages.task_done()
                        #print("FOLLOWER.RCVMSG: " + str(message) + "\n")
                        if receivedHeartbeat(message): #no append entries, just a heartbeat, so reset timer
                            print("Reset FOLLOWER TIMEOUT.\n")
                            resetFollowerTimeout()
                            updateLeader(message)
                        elif message['request_vote_flag'] == 1 and canSendVote(message['term']): #request vote sent in message, so respond to it. check to see if already voted in term
                            print("Vote REQUESTED, Sending REPLY so RESET TIMER\n")
                            sendVoteReply(message, self_id)
                            resetFollowerTimeout()
                        elif message['request_command_vote_flag'] == 1:
                            print("COMMAND Vote REQUESTED, Sending COMMAND VOTE REPLY so RESET TIMER\n")
                            sendCommandVoteReply(message, self_id)
                            resetFollowerTimeout()
                            resetFollowerTimeout()
                        elif message['client_flag'] == 1 and message['request_command_vote_flag'] == null_value:
                            print("CLIENT COMMAND HEARD. Rerouting to leader.\n")
                            rerouteToLeader(message) #We can just forward the message to the leader, dont have to respond to client
                            lastTimestamp = message['command_timestamp']
                            resetFollowerTimeout()
                        elif message['commit_flag'] == 1:
                            print("COMMIT FLAG HEARD. Committing entries to log.\n")
                            commitEntries(message, self_id)
                            resetFollowerTimeout()
                            updateLeader(message)
                        '''elif message['append_entries_flag'] == 1:
                            print("FOLLOWER TIMEOUT so RESET TIMER.\n")
                            sendAppendEntriesReply()
                            resetFollowerTimeout()
                            updateLeader(message)'''


                        # Add case for replying and routing to leader

                        # Add case for voting for commit log entry

                        # Add case for listening for vote commit reply, and commit to log based on that

                        # Add capability to recover old log from permanent storage (which can then be used as a basis for catching up with the leader)
                else: #timeout happened, so transition to candidate
                    print("FOLLOWER TIMEOUT so FOLLOWER -> CANDIDATE. \n")
                    state = "CANDIDATE"
                    resetCandidateTimeout()
            #start_time = time.time()
            if state == "CANDIDATE":
                #print("In candidate state")
                #start_time = time.time()
                if noTimeout(candidate_timeout, candidate_start_time):
                    #print("Requesting vote\n")
                    RequestVote(currentTerm, null_value, null_value, null_value, self_id)
                    while messages.empty() == False:
                        message = messages.get_nowait() #TRY TO MAKE THIS get_nowait()
                        #messages.task_done()
                        #print("CANDIDATE.RCVMSG: " + str(message) + "\n")
                        if voteGranted(message, currentTerm):
                            if not candidateEntriesQueue.empty():
                                sendAllCandidatesToLeader()
                            print("Vote granted. Becoming leader.\n")
                            state = "LEADER"
                        elif receivedHeartbeat(message):
                            state = "FOLLOWER"
                            if not candidateEntriesQueue.empty():
                                sendAllCandidatesToLeader()
                            updateLeader(message)
                            print("CANDIDATE -> FOLLOWER.\n")
                            #print("In FOLLOWER state\n")
                            resetFollowerTimeout()
                        elif message['term'] > currentTerm and message['term'] is not null_value:
                            currentTerm = message['term']
                            print("CANDIDATE.currentTerm < RECVMSG.term so CANDIDATE -> FOLLOWER.\n")
                            state = "FOLLOWER"
                            if not candidateEntriesQueue.empty():
                                sendAllCandidatesToLeader()
                            updateLeader(message)
                            resetFollowerTimeout()
                        elif message['client_flag'] == 1 and message['request_command_vote_flag'] == null_value:
                            pass
                            #candidateEntriesQueue.add(message['entries'])
                        # Add case for, if receive client command, queue it, and deal with it if we become leader
                        # Add case for, if receive client command, and we receive heartbeat and go to follower state,
                else:
                    print("CANDIDATE TIMEOUT so REQUESTING VOTE with term = " + str(currentTerm) + "\n")
                    currentTerm += 1
                    RequestVote(currentTerm, null_value, null_value, null_value, self_id)
                    resetCandidateTimeout()
            #start_time = time.time()
            if state == "LEADER":
                leader_id = self_id
                leaderIdQueue.queue.clear()
                leaderIdQueue.put(leader_id)
                while messages.empty() == False:
                    message = messages.get_nowait() #TRY TO MAKE THIS get_nowait()
                    #messages.task_done()
                    #print("LEADER.RCVMSG: " + str(message) + "\n")
                    if message['term'] > currentTerm and message['term'] is not null_value:
                        currentTerm = message['term']
                        print("LEADER.currentTerm < RECVMSG.term so LEADER -> FOLLOWER.\n")
                        state = "FOLLOWER"
                        resetFollowerTimeout()
                        updateLeader(message)
                    elif message['client_flag'] == 1:
                        requestCommandVote(currentCommandTerm, null_value, null_value, null_value, self_id, message, lastTimestamp)
                        currentCommandTerm +=1
                        lastTimestamp += 1
                    elif commandVoteGranted(message, currentCommandTerm):
                        sendCommitResponse(message, lastTimestamp, self_id)
                        commitEntries(message, self_id)

                    #elif receivedEntries():
                    #    AppendEntries(term, self_id, prevLogIndex, prevLogTerm, entries, leaderCommit)
                    #elif receivedCommittedEntries():
                    #    commitEntries()
                    # add case for hearing from client
                    #    1) Send RequestVote to everyone for client command (need RequestLogVoteFlag???)
                    #    2) Listen for RequestVote response, if majority heard from, reply with Commit Message (need commit flag in RaftMessage)
                    #    3) When sending Commit Message (when majority heard from), add event to log (Raft message may need to be expanded)
                    # add case for catching up clients (Will have to add more fields to RaftMessage)
                    # Add case for popping from CandidateQueue (or work into other cases)
                    #
                    # What to do if we receive client command, and message['term'] > currentTerm???
                    # Do we ignore the client command (so command is lost) and just transition to currentTerm?
                    # If we send out a requestVote for the log entry, then who deals with adding that entry to the log after we transition???
                    # What happens if we receive client command, send out request vote, and then crash before we add it to the log??
                    # If message lost, we need client to resend commands until they're committed
                    #
                    # Add capability to add committed entries to logQueue for use in feedback in the input thread
                    #
                    # Add capability to write log to permanent storage
                else:
                    print("I am leader. My ID is " + str(self_id))
                    sendHeartbeat(self_id)
                    #messages.task_done()
                    time.sleep(heartbeat_wait_duration/ 1000.0)
        time.sleep(thread_tickrate/ 1000.0)

def canSendVote(received_term):
    if received_term not in votes_sent:
        votes_sent.add(received_term)
        return True
    else:
        print("Already voted in term " + str(received_term) + "\n")
        return False



def AppendEntries(term, leaderID, prevLogIndex, prevLogTerm, entries, leaderCommit):
    pass

def RequestVote(term, candidateID, lastLogIndex, lastLogTerm, self_id):
    other_nodes = nodes.copy()
    del other_nodes[self_id]

    for node in other_nodes:
        request_vote_flag = 1
        #print("Request vote line passed.")
        request_vote_msg = RaftMessage(self_id, request_vote_flag, null_value,
                                       null_value, null_value, term,
                                       null_value, null_value, null_value,
                                       null_value, null_value, null_value,
                                       null_value, null_value, null_value,
                                       null_value, null_value, null_value,
                                       null_value, null_value).to_dict()
        #print("Raft message line passed.")
        request_vote_msg_str = str(request_vote_msg)
        #print("Str conversion passed")
        #sending_queue.put((nodes[node], request_vote_msg_str))
        ds_1_message.send_message(nodes[node], request_vote_msg_str)

    return

'''def send(self_region, self_id):
    self_thread = threading.Timer(.100, send, [self_region, self_id]).start()
    global sending_queue
    while sending_queue.empty() == False:
        message = sending_queue.get_nowait()
        sender_id = message[0]
        message_str = message[1]
        ds_1_message.send_message(sender_id, message_str)
        #messages.put(eval(received_message))
    #messages = [eval(message) for message in received]
    return'''

'''def send_message_thread(target, str_msg):
    message_thread = threading.Thread(target = ds_1_message.send_message, args = [target, str_msg])
    message_thread.start()
    return'''

def resetFollowerTimeout():
    global follower_start_time
    global follower_timeout
    follower_start_time = time.time()
    follower_timeout = float(randint(follower_min_time_thresh, follower_max_time_thresh)) / float(1000)
    print("Timeout is now set to occur at " + str(follower_timeout) + " seconds\n")
    return

def resetCandidateTimeout():
    global candidate_start_time
    global candidate_timeout
    candidate_start_time = time.time()
    candidate_timeout = float(randint(candidate_min_time_thresh, candidate_max_time_thresh)) / float(1000)
    print("Timeout is now set to occur at " + str(candidate_timeout) + " seconds\n")
    return


def noTimeout(timeout, start_time):
    global timeout_flag
    #print(str(time.time() - start_time))
    #print(timeout)
    if time.time() - start_time < timeout or timeout_flag is True:
        #print("Timed out.")
        timeout_flag = False
        return True
    else:
        return False

class RaftMessage:
    def __init__(self, node_src, request_vote_flag, send_vote_flag, append_entries_flag,
                 entries, term, leader_region, leader_id, attack_left_event,
                 attack_right_event, block_left_event, block_right_event, from_player_region,
                 from_player_id, client_flag, request_command_vote_flag, send_command_vote_flag,
                 command_timestamp, win_flag, commit_flag):
        self.node_src = node_src
        self.request_vote_flag = request_vote_flag
        self.send_vote_flag = send_vote_flag
        self.append_entries_flag = append_entries_flag
        self.entries = entries
        self.term = term
        self.leader_region = leader_region
        self.leader_id = leader_id
        self.attack_left_event = attack_left_event
        self.attack_right_event = attack_right_event
        self.block_left_event = block_left_event
        self.block_right_event = block_right_event
        self.from_player_region = from_player_region
        self.from_player_id = from_player_id
        self.client_flag = client_flag
        self.request_command_vote_flag = request_command_vote_flag
        self.send_command_vote_flag = send_command_vote_flag
        self.command_timestamp = command_timestamp
        self.win_flag = win_flag
        self.commit_flag = commit_flag

    '''This function turns the class into a dict, for easy sending and retrieval over the Internet.'''
    def to_dict(self):
        return {'node_src' : self.node_src,
                  'request_vote_flag': self.request_vote_flag,
                  'send_vote_flag' : self.send_vote_flag,
                  'append_entries_flag': self.append_entries_flag,
                  'entries': self.entries,
                  'term' : self.term,
                  'leader_region': self.leader_region,
                  'leader_id' : self.leader_id,
                  'attack_left_event' : self.attack_left_event,
                  'attack_right_event': self.attack_right_event,
                  'block_left_event' : self.block_left_event,
                  'block_right_event' : self.block_right_event,
                  'from_player_region' : self.from_player_region,
                  'from_player_id' : self.from_player_id,
                  'client_flag' : self.client_flag,
                  'request_command_vote_flag' : self.request_command_vote_flag,
                  'send_command_vote_flag': self.send_command_vote_flag,
                  'command_timestamp' : self.command_timestamp,
                  'win_flag' : self.win_flag,
                  'commit_flag' : self.commit_flag}


'''Receive runs every .100 seconds in its own thread. Actions are done depending on message received and current state.'''
def receive(self_region, self_id):
    self_thread = threading.Timer(.100, receive, [self_region, self_id]).start()
    global run_flag
    if (run_flag):
        #self_thread.daemon = True
        received = ds_1_message.receive_message(self_region)
        global messages
        for received_message in received:
            messages.put(eval(received_message))
    else:
        received = ds_1_message.receive_message(self_region)
    #messages = [eval(message) for message in received]

def clearMessages(self_region, self_id):
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    ds_1_message.receive_message(self_region)
    time.sleep(.150)
    return


def sendVoteReply(message, self_id):
    vote_reply_flag = 1
    vote_response = RaftMessage(self_id, null_value, vote_reply_flag,
                                null_value, null_value, currentTerm,
                                null_value, null_value, null_value,
                                null_value, null_value, null_value,
                                null_value, null_value, null_value,
                                null_value, null_value, null_value,
                                null_value, null_value).to_dict()
    vote_response_str = str(vote_response)
    candidate = nodes[message['node_src']]
    #sending_queue.put((candidate, vote_response_str))
    ds_1_message.send_message(candidate, vote_response_str)
    return

def receivedHeartbeat(message):
    if message['entries'] == 0:
        print("Received heartbeat.\n")
        return True
    else:
        return False

def receivedEntries(message):
    if message['entries']:
        print("Received entries. Resetting timeout")
    return

def voteGranted(message, currentTerm):
    global votes_received
    global voters_received
    if message['send_vote_flag'] == 1 and message['node_src'] not in voters_received:
        voters_received.add(message['node_src'])
        print("Received vote. CURRENT VOTES: " + str(votes_received) + "\n")
        votes_received += 1
    if votes_received >= majority_vote and currentTerm not in alreadyWonSet:
        alreadyWonSet.add(currentTerm)
        print("VOTE WON: Majority of votes received in term " + str(currentTerm) + "\n")
        votes_received = 1 #reset votes
        voters_received.clear()
        return True

def commitEntries(message, self_id):
    global logEntriesQueue
    global player_self_id_global

    command = ""
    target = ""
    timestamp = message['command_timestamp']
    isConsumed = True
    if message['attack_left_event'] is not null_value:
        command = "attack_left"
        target = message['attack_left_event']
    if message['attack_right_event'] is not null_value:
        command = "attack_right"
        target = message['attack_right_event']
    if message['block_left_event'] is not null_value:
        command = "block_left"
        target = message['block_left_event']
    if message['block_right_event'] is not null_value:
        command = "block_right"
        target = message['block_right_event']
    if message['win_flag'] is not null_value:
        command = "win"
        target = message['win_event']

    entryList = [command,target,timestamp,isConsumed]
    print("Committing " +str(entryList) + " to log.")
    logEntriesQueue.put(entryList)
    entryListStr = str(entryList)
    ds_1_storage.to_file(entryListStr, self_id)
    ds_1_storage.to_s3(nodes[self_id], self_id)

    #print("Entire Log is:\n " + str(ds_1_storage.from_file(self_id)))
    print("Entire log is: ")
    eval_log_entries = eval(str(ds_1_storage.from_file(self_id)))
    for index, value in enumerate(eval_log_entries):
        print(str(value))

    getFeedback3(logEntriesQueue, player_self_id_global)




def sendHeartbeat(self_id):
    print("LEADER SEND HEARTBEAT\n")
    other_nodes = nodes.copy()
    del other_nodes[self_id]
    entries = 0

    for node in other_nodes:
        heartbeat_msg = RaftMessage(self_id, null_value, null_value,
                                    null_value, entries, currentTerm,
                                    null_value, self_id, null_value,
                                    null_value, null_value, null_value,
                                    null_value, null_value, null_value,
                                    null_value, null_value, null_value,
                                    null_value, null_value).to_dict()
        heartbeat_msg_str = str(heartbeat_msg)
        #sending_queue.put((other_nodes[node], heartbeat_msg_str))
        ds_1_message.send_message(other_nodes[node], heartbeat_msg_str)

    return

if __name__ == '__main__':
    print(sys.argv)
    '''Some OS may interpret the client.py as a parameter, offsetting the proper argv index'''
    if str(sys.argv[0]).find("ds_1_driver.py") == -1:
        region_self_string = sys.argv[0]
        region_1_string = sys.argv[1]
        region_1_id = sys.argv[2]
        region_2_string = sys.argv[3]
        region_2_id = sys.argv[4]
        region_3_string = sys.argv[5]
        region_3_id = sys.argv[6]
        region_4_string = sys.argv[7]
        region_4_id = sys.argv[8]
        node_id_number = sys.argv[9]
        try:
            player_1_region = sys.argv[10]
            player_1_id = sys.argv[11]
            player_2_region = sys.argv[12]
            player_2_id = sys.argv[13]
            player_self_id = sys.argv[14]
        except IndexError:
            player_1_region = "none"
            player_1_id = "none"
            player_2_region = "none"
            player_2_id = "none"
            player_self_id = "none"
    else:
        region_self_string = sys.argv[1]
        region_1_string = sys.argv[2]
        region_1_id = sys.argv[3]
        region_2_string = sys.argv[4]
        region_2_id = sys.argv[5]
        region_3_string = sys.argv[6]
        region_3_id = sys.argv[7]
        region_4_string = sys.argv[8]
        region_4_id = sys.argv[9]
        node_id_number = sys.argv[10]
        try:
            player_1_region = sys.argv[11]
            player_1_id = sys.argv[12]
            player_2_region = sys.argv[13]
            player_2_id = sys.argv[14]
            player_self_id = sys.argv[15]
        except IndexError:
            player_1_region = "none"
            player_1_id = "none"
            player_2_region = "none"
            player_2_id = "none"
            player_self_id = "none"

    controller(region_self_string, region_1_string, region_1_id, region_2_string, region_2_id, region_3_string, region_3_id, region_4_string, region_4_id, node_id_number, player_1_region, player_1_id, player_2_region, player_2_id, player_self_id)
    #ds_1_storage.tester_func(region_self_string)
