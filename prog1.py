import boto3, uuid, sys, time, ds_1_message, ds_1_input, ds_1_driver, ds_1_storage

def aws_network_setup(region_self, region1, region2, region3, self_number):
	# Create our S3 storage for the log, and SQS queue

	# Make client for our S3 storage
	self_s3_client = boto3.client('s3')

	# Check for bucket, if it already exists, delete it and remake it (workaround for some S3 weirdness)
	self_bucket = boto3.resource('s3')
	#self_bucket.create(name='')
	bucket = self_bucket.create_bucket(CreateBucketConfiguration = {'LocationConstraint' : region_self}, Bucket='bigboi-unique-bucket-ds-spring-2018-bucket')


	self_queue = boto3.resource('sqs')
	queue = self_queue.create_queue(QueueName='node', Attributes={'DelaySeconds':'1', 'ReceiveMessageWaitTimeSeconds':'2'})

	# Make self client for sqs
	self_sqs_client = boto3.client('sqs', region_name = region_self)
	

		
	# Wait a short while before trying to connect to other queues
	time.sleep(5)

	# Connect to the other nodes
	node_1 = boto3.client('sqs', region_name=region1)
	node_2 = boto3.client('sqs', region_name=region2)
	node_3 = boto3.client('sqs', region_name=region3)

	# Get queue addresses from all nodes including self
	node_self_url = self_sqs_client.get_queue_url(QueueName='node')['QueueUrl']
	node_1_url = node_1.get_queue_url(QueueName='node')['QueueUrl']
	node_2_url = node_2.get_queue_url(QueueName='node')['QueueUrl']
	node_3_url = node_3.get_queue_url(QueueName='node')['QueueUrl']

	# Send messages to each other node
	'''node_1.send_message(QueueUrl=node_1_url, MessageBody="hello " + region1 + " from node " + self_number + " in " + str(region_self))
	node_2.send_message(QueueUrl=node_2_url, MessageBody="hello " + region2 + " from node " + self_number + " in " + str(region_self))
	node_3.send_message(QueueUrl=node_3_url, MessageBody="hello " + region3 + " from node " + self_number + " in " + str(region_self))'''
		
	end_time = time.time() + 60 * .2
	
	# Receive messages for a short while
	#while (time.time() < end_time):

	time.sleep(1)
	received_messages_raw = self_sqs_client.receive_message(QueueUrl=node_self_url, WaitTimeSeconds=5, MaxNumberOfMessages=10)



	message = {self_number: {{'name' : appt_name}, {'date': appt_date}, {'start_time': appt_start}, {'end_time' : appt_end}, {'participants': appt_participants}}}
	message_str = str(message)

	if 'Messages' in received_messages_raw:
		received_messages = self_sqs_client.receive_message(QueueUrl=node_self_url, WaitTimeSeconds=5, MaxNumberOfMessages=10)['Messages']
		for message in range(len(received_messages)):
			if 'Body' in received_messages[message]:
				print(received_messages[message]['Body'])
			else:
				print("No message body")
	else:
		print("No messages")
	#self_s3_client.put_object(Body=received_message)

	#s3_received_message = self_s3_client.get_object(Bucket='bigboi-unique-bucket-ds-spring-2018-bucket')

	#print("Message in S3 is " + str(s3_received_message))



class Calendar:
	def __init__(self):
		self.calendar = {}

	def add(self, appointment):
		self.calendar.update(appointment)

	def remove(self, appointment_name):
		self.calendar.pop(appointment_name)

class Appointment:
	def __init__(self, name, date, start_time, end_time, participants):
		self.name = name
		self.data = date
		self.start_time = start_time
		self.end_time = end_time
		self.participants = participants

	def add_participant(self, participant):
		if not self.is_conflict():
			self.participants.add(participant)

	def remove_participant(self, participant):
		self.participants.pop(participant)

	def is_conflict(self):
		pass

class User:
	def __init__(self):
		self.name = "person_" + " " + str(uuid.uuid1())

# Parse the region names from the command line
if __name__ == '__main__':
	print(sys.argv)
	'''Some OS may interpret the client.py as a parameter, offsetting the proper argv index'''
	if str(sys.argv[0]).find("prog1.py") == -1:
		region_self_string = sys.argv[0]
		region_1_string = sys.argv[1]
		region_2_string = sys.argv[2]
		region_3_string = sys.argv[3]
		node_id_number = sys.argv[4]
	else:
		region_self_string = sys.argv[1]
		region_1_string = sys.argv[2]
		region_2_string = sys.argv[3]
		region_3_string = sys.argv[4]
		node_id_number = sys.argv[5]

	aws_network_setup(region_self_string, region_1_string, region_2_string, region_3_string, node_id_number)

