import boto3,time

'''Takes a dict message in, and converts it to a string, sending it to the target region using an SQS queue'''
def send_message(region, message):
    start_time = time.time()
    session = boto3.session.Session()
    node_client = session.client('sqs', region_name=region)
    node_url = node_client.get_queue_url(QueueName='node')['QueueUrl']
    #print("Message in send_message is " + str(message))
    node_client.send_message(QueueUrl=node_url, MessageBody=str(message))
    end_time = time.time() - start_time
    #print("End time in send_message is " + str(end_time))
    return

'''Receives a message from an SQS queue in the input region'''
def receive_message(region_self):
    start_time = time.time()
    session = boto3.session.Session()
    self_sqs = session.resource('sqs', region_name=region_self)
    self_queue = self_sqs.get_queue_by_name(QueueName='node')
    messages_to_delete = []
    messages_to_return = []
    self_sqs_client = session.client('sqs', region_name = region_self)
    node_self_url = self_sqs_client.get_queue_url(QueueName='node')['QueueUrl']
    received_messages_raw = self_sqs_client.receive_message(QueueUrl=node_self_url, WaitTimeSeconds=5, MaxNumberOfMessages=10)
    #print(received_messages_raw)
    if 'Messages' in received_messages_raw:
        received_messages = received_messages_raw['Messages']
        #received_messages = self_sqs_client.receive_message(QueueUrl=node_self_url, WaitTimeSeconds=5, MaxNumberOfMessages=10)['Messages']
        for message in range(len(received_messages)):
            if 'Body' in received_messages[message]:
                message_body = received_messages[message]['Body']
                #print("Message received.\n")
                #print("Message received in ds_1_message.py is " + str(received_messages[message]['Body']) + "\n")
                #print(received_messages[message]['ReceiptHandle'])
                messages_to_return.append(message_body)
            else:
                pass
                #print("No message body")
        messages_to_delete.append({'Id': received_messages[message]['MessageId'],'ReceiptHandle': received_messages[message]['ReceiptHandle']})
    else:
        pass
		#print("No messages")

    if len(messages_to_delete) == 0:
        pass
    # send delete response to remove successfuly received messages from the SQS queue
    else:
        delete_response = self_queue.delete_messages(Entries=messages_to_delete)
    end_time = time.time() - start_time
    #print("End time in receive_message is " + str(end_time))
    return messages_to_return


