'''The function sets up an Appointment class using command line input.'''
def get_appt_input(time_stamp, time_table):
	formatted_input_str = raw_input("Enter name date start_time end_time in that order separated by spaces")
	formatted_input_str_array = formatted_input_str.split()
	appt_name = formatted_input_str_array[0]
	appt_date = formatted_input_str_array[1]
	appt_start = formatted_input_str_array[2]
	appt_end= formatted_input_str_array[3]

	formatted_part_input_str = raw_input("Enter participants delineated with spaces")
	formatted_part_input_str_array = formatted_part_input_str.split()
	appt_participants = []
	for i in range(len(formatted_part_input_str_array)):
		appt_participants.append(formatted_part_input_str_array[i])

	return Appointment(appt_name, appt_date, appt_start, appt_end, appt_participants, time_stamp, time_table, 0, 0)

'''An appointment class that is returned by the get_appt_input function. In the current implementation,
the class serves as both the appointment class, and a message class. All messages have all fields. The name, date,
start_time, end_time, and participants are for the messages. The timestamp and time_table are for WuuBernstein,
and the denied and removal flags are for conflict/ removal resolution.'''
class Appointment:
	def __init__(self, name, date, start_time, end_time, participants, timestamp, time_table, denied, removed):
		self.name = name
		self.date = date
		self.start_time = start_time
		self.end_time = end_time
		self.participants = participants
		self.timestamp = timestamp
		self.time_table = time_table
		self.denied = denied
		self.removed = removed

	'''This function turns the class into a dict, for easy sending and retrieval over the Internet.'''
	def to_dict(self, node_number):
		return {node_number: {'name' : self.name,
							  'date': self.date,
							  'start_time': self.start_time,
							  'end_time' : self.end_time,
							  'participants': self.participants,
							  'timestamp' : self.timestamp,
							  'time_table' : self.time_table,
							  'denied' : self.denied,
							  'removed': self.removed}}

	def get_participants(self):
		return self.participants

'''The debugging function to test internal script functionality.'''
def self_debugger():
	apt_1 = get_appt_input()
	dict = apt_1.to_dict(1)
	print(str(dict))
		
#self_debugger()
