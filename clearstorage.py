import ds_1_storage

'''A function used for debugging- can clear all local storage.'''
def clearfile():
    for x in range(0,4):
        ds_1_storage.clear_file(x)
		
clearfile()
